//
//  DataHelper.swift
//  PitchPerfect
//
//  Created by Victor Yang on 27/11/2017.
//  Copyright © 2017 Victor Yang. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth

var contentStrLog:String = ""

class DataHelper: NSObject {
    
    var ref: DatabaseReference!
    var startByte: UInt8!
    var dataCountdown: Int!
    var cipher = [UInt8]()
    var content = [UInt8]()
    var checkSum: UInt8!
    
    var decryptedContent = [UInt8]()
    var contentStr:String!
    var dataDictionary:Dictionary<String, Any>!
    
    var timestamp:Int!
    
    override init() {
        
        super.init()
        flushOutData()
        ref = Database.database().reference()


    }
    
    func dataArrived(data:[UInt8]) -> Bool{
        debugPrint("Raw Data: \(data)")
        if data[0] == 65 {
            startGettingData(data: data)

            return dataCountdown <= 0
            
        }else{
            for value in data {
                appendToContent(byte: value)
            }
            
            return dataCountdown <= 0
        }
    }
    
    func startGettingData(data:[UInt8]) {
        startByte = data[0]
        dataCountdown = Int(data[1]) - 8
        debugPrint("dataCountdown is \(dataCountdown)")
        cipher = [UInt8](data[2...9])
        
        for value in data[10...] {
            appendToContent(byte: value)
        }
    }
    
    func appendToContent(byte:UInt8) {
        
        dataCountdown = dataCountdown - 1
        if startByte == 65{
            if dataCountdown == 0{
                checkSum = byte
            }else if dataCountdown > 0{
                content.append(byte)
            }
//            debugPrint("append: \(byte), countdown at \(dataCountdown)")
        }
        
    }
    
    func processData() {
        if checkWithChecksum(dataToCheck: content + cipher, checkSum: checkSum){
            decryptedContent = decrypContent(contentToDecrypt: content)
        
        debugPrint(decryptedContent)
        
        
        contentStr = String(convertRawArrToCharArr(rawData: decryptedContent))
            dataDictionary = convertToDictionary(text: contentStr)!
        }
    }
    
    
    func sendDataToFirebase() {
        
        timestamp = Int(NSDate().timeIntervalSince1970 * 1000.0)
        
        if !dataDictionary.isEmpty {
            logOnFirebaseDB(data: dataDictionary, timestamp: timestamp)
            createLogStrFromContent(rawContent: content, decryptedContent: decryptedContent, contentStr: contentStr, timestamp: timestamp)
        }
        
    }
    
    private func checkWithChecksum(dataToCheck:[UInt8], checkSum:UInt8) -> Bool {
        var sum:Int = 0
        for value in dataToCheck {
            sum = (sum + Int(value))
        }
        
        let calSum = UInt8 (sum & 0x00ff)
    
        debugPrint("sum is \(calSum), checkSum is \(checkSum)")
        
        return calSum == checkSum
    }
    
    private func decrypContent(contentToDecrypt:[UInt8]) -> [UInt8] {
        var decrypedContent = [UInt8]()
        
        for (i, t) in contentToDecrypt.enumerated() {
            decrypedContent.append(t ^ cipher[i%8])
        }
        
        return decrypedContent
    }
    
    private func convertRawArrToCharArr(rawData:[UInt8]) -> [Character] {
        var charArr = [Character]()
        for value in rawData {
            charArr.append(Character(UnicodeScalar(value)))
        }
        
        return charArr
    }
    
    
    private func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                debugPrint(error.localizedDescription)
            }
        }
        return [String: String]()
    }
    
    private func createLogStrFromContent(rawContent:[UInt8], decryptedContent:[UInt8], contentStr:String, timestamp:Int) {
        
        
        var log = "--------------------\(timestamp)-----------------------\n"
        log.append("<Raw Data>\n")
        log.append("contentCombined:  \(rawContent)\n")
        log.append("jsonContent:  \(decryptedContent)\n")
        log.append("<String Data>\n")
        log.append("\(contentStr)\n")
        log.append("-----------------------END-------------------------\n\n\n")
        
        print(log)
        
        log.append(contentStrLog)
        contentStrLog = log
    }
    
    
    
    private func logOnFirebaseDB(data:[String: Any], timestamp:Int) {
        if data.count > 3 {
            
//            var randomSyss = [Int]()
//            var randomDias = [Int]()
//
//            let numOfrandom = 100
//
//            for _ in 0...numOfrandom{
//                randomDias.append(Int(arc4random_uniform(40)+90))
//            }
//
//            for _ in 0...numOfrandom{
//                randomSyss.append(Int(arc4random_uniform(30)+60))
//            }
//
//            let uid = Auth.auth().currentUser?.uid
//            var newts = timestamp
//            for index in randomSyss.enumerated(){
//
//                newts = newts + index.element
//                let reading = ["Id"   : data["Id"] as! String,
//                               "Sys": String(randomSyss[index.offset]),
//                               "Dia" : String(randomDias[index.offset]),
//                               "Pulse"  : data["Pulse"] as! String]
//                let childUpdates = ["/\(newts)": reading]
//                ref.child("readings").child(uid!).updateChildValues(childUpdates)
//            }
            
            
//            /////////////////////
            let uid = Auth.auth().currentUser?.uid
            //            let key = ref.child("readings").child(uid!).childByAutoId().key
            let reading = ["Id"   : data["Id"] as! String,
                           "Sys": data["Sys"] as! String,
                           "Dia" : data["Dia"] as! String,
                           "Pulse"  : data["Pulse"] as! String]
            let childUpdates = ["/\(timestamp)": reading]
            ref.child("readings").child(uid!).updateChildValues(childUpdates)
            
        }
    }
    
    func flushOutData(){
         startByte = 0
         dataCountdown = 0
         cipher = [UInt8]()
         content = [UInt8]()
         checkSum = 0
        
         decryptedContent = [UInt8]()
         contentStr = ""
         dataDictionary = Dictionary()
        
         timestamp = 0
    }
    
}
