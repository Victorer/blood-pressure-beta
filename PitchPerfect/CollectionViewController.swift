//
//  CollectionViewController.swift
//  PitchPerfect
//
//  Created by Victor Yang on 12/11/2017.
//  Copyright © 2017 Victor Yang. All rights reserved.
//

import UIKit
import FirebaseAuth

class CollectionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ArrayUpdateDelegate {
    
    // Bean access
    let bean = Bean()
    var handle: AuthStateDidChangeListenerHandle?

    @IBOutlet weak var logArea: UITextView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        bean.arrayUpdateDelegate = self
        bean.searchPeripheral()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            if user?.uid == nil {
                contentStrLog = ""
                self.goToLogin()
            }else{
                
                self.logArea.text = contentStrLog
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        Auth.auth().removeStateDidChangeListener(handle!)
        array = [String]()
        tableView.reloadData()
    }
    
    @IBAction func clickSignOut(_ sender: UIButton) {
        debugPrint("Click Sign Out")
        do {
            try Auth.auth().signOut()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
        cell.textLabel?.text = array[indexPath.row]
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if array.count > indexPath.row {
            BEAN_NAME = array[indexPath.row]
            performSegue(withIdentifier: "toConnected", sender: self)        }
    }
    
    func didUpdated(content: String) {
        tableView.reloadData()
    }
    
    
    func goToLogin(){
    
    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let newViewController = storyBoard.instantiateViewController(withIdentifier: "loginView") as! LoginController
    self.present(newViewController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? ViewController {
            destinationViewController.bean = bean
        }
    }
    
}
