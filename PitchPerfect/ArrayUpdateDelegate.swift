//
//  ArrayUpdateDelegate.swift
//  PitchPerfect
//
//  Created by Victor Yang on 12/11/2017.
//  Copyright © 2017 Victor Yang. All rights reserved.
//

import Foundation

protocol ArrayUpdateDelegate {
    func didUpdated(content:String)
}
