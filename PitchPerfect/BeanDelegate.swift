//
//  BeanDelegate.swift
//  PitchPerfect
//
//  Created by Victor Yang on 23/11/2017.
//  Copyright © 2017 Victor Yang. All rights reserved.
//

import Foundation

protocol BeanDelegate {
    func didGet(content:[UInt8])
    func didConnected(content:String)
    func didFoundBLE(content:String)
    func didDisconnected(content:String)
}
