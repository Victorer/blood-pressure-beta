//
//  Bean.swift
//  PitchPerfect
//
//  Created by Victor Yang on 23/11/2017.
//  Copyright © 2017 Victor Yang. All rights reserved.
//

import CoreBluetooth
import Foundation


var array = [String]()
var BEAN_NAME = ""

class Bean: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    // Delegate
    var delegate: BeanDelegate?
    var arrayUpdateDelegate: ArrayUpdateDelegate?
    
    // Beacon constants
    let BEAN_SCRATCH_UUID = CBUUID(string: "FFE1")
    let BEAN_SERVICE_UUID = CBUUID(string: "FFE0")
    
    // Beacon management
    var manager:CBCentralManager!
    var peripheral:CBPeripheral!
    
    var prepareToConnect = false
    
    override init() {
        super.init()
        
        // Beacon manager
        manager = CBCentralManager(delegate: self, queue: nil)
    }
    
    func searchPeripheral() {
        
        // Is Bluetooth even on
        if manager.state == CBManagerState.poweredOn {
            // Start looking
            manager.scanForPeripherals(withServices: nil, options: nil)
            
            // Debug
            debugPrint("Searching ...")
        } else {
            // Bzzt!
            debugPrint("Bluetooth not available.")
        }
    }
    
    func searchPeripheralAndPrepareToConnect() {
        prepareToConnect = true
        
        // Is Bluetooth even on
        if manager.state == CBManagerState.poweredOn {
            // Start looking
            manager.scanForPeripherals(withServices: nil, options: nil)
            
            // Debug
            debugPrint("Searching ...")
        } else {
            // Bzzt!
            debugPrint("Bluetooth not available.")
        }
    }
    
    // Check Bluetooth configuration on the device
    func centralManagerDidUpdateState(_ central:CBCentralManager) {
        
        // Is Bluetooth even on
        if central.state == CBManagerState.poweredOn {
            // Start looking
            central.scanForPeripherals(withServices: nil, options: nil)
            
            // Debug
            debugPrint("Searching ...")
        } else {
            // Bzzt!
            debugPrint("Bluetooth not available.")
        }
    }
    
    
    func connectPeripheral() {
        
        // Connect to the perhipheral proper
        manager.connect(peripheral, options: nil)
    }
    
    // Found a peripheral
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        // Device
        let device = (advertisementData as NSDictionary).object(forKey: CBAdvertisementDataLocalNameKey) as? NSString
        
        if(prepareToConnect){
            if device?.contains(BEAN_NAME) == true {
                // Stop looking for devices
                // Track as connected peripheral
                // Setup delegate for events
                self.manager.stopScan()
                self.peripheral = peripheral
                self.peripheral.delegate = self
                delegate?.didFoundBLE(content: BEAN_NAME)
                connectPeripheral()
                
                // Debug
                debugPrint("Found Bean.")
            }
        }else{
            if(device != nil){
                if(!array.contains(device! as String)){
                    array.append(device! as String)
                    debugPrint(device! as String)
                }
                arrayUpdateDelegate?.didUpdated(content: device! as String)

            }
        }
    }
    
    // Connected to peripheral
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        // Ask for services
        self.peripheral.discoverServices(nil)
        
        // Debug
        debugPrint("Getting services ...")
    }
    
    
    // Discovered peripheral services
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        // Look through the service list
        for service in peripheral.services! {
            let thisService = service as CBService
            
            // If this is the service we want
            if service.uuid == BEAN_SERVICE_UUID {
                // Ask for specific characteristics
                self.peripheral.discoverCharacteristics(nil, for: thisService)
                
                // Debug
                debugPrint("Using scratch.")
            }
            
            // Debug
            debugPrint("Service: ", service.uuid)
        }
    }
    
    // Discovered peripheral characteristics
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        debugPrint("Enabling ...")
        // Look at provided characteristics
        for characteristic in service.characteristics! {
            let thisCharacteristic = characteristic as CBCharacteristic
            
            // If this is the characteristic we want
            if thisCharacteristic.uuid == BEAN_SCRATCH_UUID {
                
                
                // Start listening for updates
                // Potentially show interface
                self.peripheral.setNotifyValue(true, for: thisCharacteristic)
                delegate?.didConnected(content: BEAN_SCRATCH_UUID.uuidString)
                // Debug
                debugPrint("Set to notify: ", thisCharacteristic.uuid)
            }
            
            // Debug
            debugPrint("Characteristic: ", thisCharacteristic.uuid)
        }
    }
    
    // Data arrived from peripheral
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
        debugPrint("data received")
        // Make sure it is the peripheral we want
        if characteristic.uuid == BEAN_SCRATCH_UUID {
            let values = [UInt8](characteristic.value!)
            
            // Delegate
            delegate?.didGet(content: values)
            debugPrint("data passed")
        }
        
    }
    
    func disconnectBLE(){
        if peripheral == nil {
        }else{
            self.manager.cancelPeripheralConnection(peripheral)
            prepareToConnect = false
        }
    }
    
    // Peripheral disconnected
    // Potentially hide relevant interface
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        debugPrint("Disconnected.")
        
        delegate?.didDisconnected(content: BEAN_NAME)
    }
    
    
}
