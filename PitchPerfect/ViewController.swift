//
//  ViewController.swift
//  PitchPerfect
//
//  Created by Victor Yang on 17/10/2017.
//  Copyright © 2017 Victor Yang. All rights reserved.
//

import UIKit



class ViewController: UIViewController, BeanDelegate {
    
    
    @IBOutlet weak var recordingLabel: UILabel!
    @IBOutlet weak var statusLable: UITextField!
    @IBOutlet weak var connectBtn: UIButton!
    @IBOutlet weak var goBackBtn: UIButton!
    
    
    // Bean access
    var bean: Bean?
    
    var dataHelper: DataHelper?
    
    // Last reading
//    var contentCombined = [UInt8]()
//    var dataCountdown:Int = 0
//    var cipher = [UInt8]()
    
    //    var receivingDataState = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        stopRecordButton.isEnabled = false
        
        // Listen for readings
        bean?.delegate = self
        statusLable.text = "Found device: \(BEAN_NAME)"
        goBackBtn.isEnabled = false
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        connectBtn.setTitle("Disconnect", for: .normal)
        statusLable.text = "Getting data from \(BEAN_NAME)"
        bean?.searchPeripheralAndPrepareToConnect()
    }
    
    @IBAction func goBackToBLList(_ sender: Any) {
        goBackToList()
    }
    
    func goBackToList(){
        bean?.disconnectBLE()
    }
    

    
    @IBAction func onConnectClick(_ sender: UIButton) {
        let statusStr: String = statusLable.text!
        
        if statusStr.contains("Found"){
            sender.setTitle("Disconnect", for: .normal)
            statusLable.text = "Getting data from \(BEAN_NAME)"
            
            bean?.searchPeripheralAndPrepareToConnect()
        }else if (sender.title(for: .normal)?.contains("Disconnect"))!{
            goBackToList()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func startRecording(_ sender: Any) {
        recordingLabel.text = "Recording in Progress"
    }
    
    @IBAction func stopRecording(_ sender: Any) {
        recordingLabel.text = "Recording has stopped"
    }
    
    // Bean received reading
    func didGet(content: [UInt8]) {
        
        debugPrint("data lenth \(content.count) is in ViewController")
        
        if dataHelper == nil {
            dataHelper = DataHelper()
        }
        
        if (dataHelper?.dataArrived(data: content))!{
            dataHelper?.processData()
            dataHelper?.sendDataToFirebase()
            dataHelper?.flushOutData()
            
            goBackToList()
        }
        
        ////////////////
        
//        if content[0] == 65 {
//            dataCountdown = Int(content[1]) + 3  //include first two byte + checksum byte
//            debugPrint("dataCountdown is \(dataCountdown)")
//
//
//            cipher = [UInt8](content[2...9])
//        }
//
        // Reference last reading
//        for value in content {
//            contentCombined.append(value)
//            dataCountdown = dataCountdown - 1
//
//            debugPrint("current dataCountdown: \(dataCountdown)")
//            if dataCountdown == 0 {
//
//                let checkSum = contentCombined[contentCombined.count-1]
//
//                //check with checksum
//                for value in
//
//
//                let lastJsonIndex = Int(contentCombined[1])
//                let jsonContent = contentCombined[10...lastJsonIndex]
//                var decrypedJson = [UInt8]()
//
//                for (i, t) in jsonContent.enumerated() {
//                    decrypedJson.append(t ^ cipher[i%8])
//                }
//
//                var jsonInChars = [Character]()
//                for value in decrypedJson {
//                    jsonInChars.append(Character(UnicodeScalar(value)))
//                }
//                let jsonStr = String(jsonInChars)
//
//                let dataDictionary:Dictionary = convertToDictionary(text: jsonStr)!
//
//
//                let timestamp = Int(NSDate().timeIntervalSince1970)
//
//                var log = "--------------------\(timestamp)-----------------------\n"
//                log.append("<Raw Data>\n")
//                log.append("contentCombined:  \(contentCombined)\n")
//                log.append("jsonContent:  \(decrypedJson)\n")
//                log.append("<String Data>\n")
//                log.append("\(jsonStr)\n")
//                log.append("-----------------------END-------------------------\n\n\n")
//
//                print(log)
//
//                log.append(contentStrLog)
//                contentStrLog = log
//
//                //            print("--------------------\(timestamp)-----------------------")
//                //            print("---Raw Data---")
//                //            print("contentCombined:  \(contentCombined)")
//                //            print("jsonContent:  \(jsonContent)")
//                //            print("---String Data---")
//                //            print(jsonStr)
//                //            print("---END---\n\n")
//
//                if !dataDictionary.isEmpty {
//                    logOnFirebaseDB(data: dataDictionary, timestamp: timestamp)
//                }
//
//                contentCombined.removeAll()
//            }
//        }
        
        
        
        
    }
    
    func didFoundBLE(content: String) {
        statusLable.text = "Found \(content)"
    }
    
    func didConnected(content: String) {
        statusLable.text = "Connected to \(content)"
        goBackBtn.isEnabled = true
    }
    
    func didDisconnected(content: String) {
        statusLable.text = "Not Connected"
        
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
//        debugPrint("startConverting")
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                debugPrint(error.localizedDescription)
            }
        }
        return [String: String]()
    }
    
}

